
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var ehrId_prvi = "";
var ehrId_drugi = "";
var ehrId_tretji = "";
var globBMR = "";

/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
window.onload = function obNalozitvi(){
	dodaj();
}
function dodaj(){
	generirajPodatke(1);
	generirajPodatke(2);
	generirajPodatke(3);
} 
function generirajPodatke(stPacienta) {
  ehrId = "";
		if(stPacienta == 1){
			var ime = "Hatshepsut";
			var priimek = "Tora";
			var teza = 70;
			var visina = 180;
			var dia = 70;
			var sis = 110;
			var datum = "1990-08-01"
		var sessId = getSessionId();
        $.ajaxSetup({
		    headers: {"Ehr-Session": sessId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datum,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (odgovor) {
		        
		            ehrId_prvi = ehrId;
		            $('#prvi').append(ime+" "+priimek+" EHR ID: "+ ehrId_prvi);
		            dodajMeritve(ehrId_prvi, visina, teza, sis, dia);
		            },
		            error: function(err) { 
		            	console.log("PRIŠLO JE DO NAPAKE!");
		            }
		        });
		    }
		});
		}
		else if(stPacienta == 2){
			var ime = "Veaceslav";
			var priimek = "Floris";
			var teza = 110;
			var visina = 175;
			var dia = 90;
			var sis = 130;
			var datum = "1983-08-01"
			var sessId = getSessionId();
        $.ajaxSetup({
		    headers: {"Ehr-Session": sessId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datum,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (odgovor) {
		        
		            ehrId_drugi = ehrId;
		            $('#drugi').append(ime+" "+priimek+" EHR ID: "+ ehrId_drugi);
		            dodajMeritve(ehrId_drugi, visina, teza, sis, dia);
		            },
		            error: function(err) { 
		            	console.log("PRIŠLO JE DO NAPAKE!");
		            }
		        });
		    }
		});
		}
		else if(stPacienta == 3){
			var ime = "Andreas";
			var priimek = "Panzis";
			var teza = 90;
			var visina = 200;
			var dia = 60;
			var sis = 100;
			var datum = "1950-07-02";
			var sessId = getSessionId();
        $.ajaxSetup({
		    headers: {"Ehr-Session": sessId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datum,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (odgovor) {
		        
		            ehrId_tretji = ehrId;
		            $('#tretji').append(ime+" "+priimek+" EHR ID: "+ ehrId_tretji);
		            dodajMeritve(ehrId_tretji, visina, teza, sis, dia);
		            },
		            error: function(err) { 
		            	console.log("PRIŠLO JE DO NAPAKE!");
		            }
		        });
		    }
		});
		}

  return ehrId;
}


function dodajanjeUporabnika(){
    if($('#ime').val() == "" || $('#priimek').val() == ""){
        alert("PREVERITE VNOSNA POLJA IN POSKUSITE PONOVNO!");
    }
    else{
        var sessId = getSessionId();
        var ime = $('ime').val();
        var priimek = $('priimek').val();
        var datumrojstva = $('rojstvo').val();
        $.ajaxSetup({
		    headers: {"Ehr-Session": sessId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumrojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (odgovor) {
		                $('#ID').val(ehrId);
		                $("#novUporabnik").append("EHR ID: "+ ehrId);
		            },
		            error: function(err) {  // neuspešno
		            	console.log("PRIŠLO JE DO NAPAKE!");
		            }
		        });
		    }
		});
    }
}
function getIndex() {
    var index = document.getElementById("select").selectedIndex;
    if(index == 1){
    	$('#ID').val(ehrId_prvi);
    	$('#visina').val(180);
		$('#teza').val(70);
		$('#sisT').val(110);
		$('#diaT').val(70);
    }
    else if(index == 2){
    	$('#ID').val(ehrId_drugi);
    	$('#visina').val(175);
		$('#teza').val(110);
		$('#sisT').val(130);
		$('#diaT').val(90);
    }
    else if(index == 3){
    	$('#ID').val(ehrId_tretji);
    	$('#visina').val(180);
		$('#teza').val(90);
		$('#sisT').val(100);
		$('#diaT').val(60);
    }
    else{
    	$('#visina').val("");
		$('#teza').val("");
		$('#sisT').val("");
		$('#diaT').val("");
    }
    
}
function dodajMeritve(ehrId, visina, teza, sis, dia){
	var sessId = getSessionId();
	$.ajaxSetup({
		    headers: {"Ehr-Session": sessId}
		});
		var podatki = {
			// Preview Structure: https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": "1957-03-10T09:08",
		    "vital_signs/height_length/any_event/body_height_length": visina,
		    "vital_signs/body_weight/any_event/body_weight": teza,
		    "vital_signs/blood_pressure/any_event/systolic": sis,
		    "vital_signs/blood_pressure/any_event/diastolic": dia,
		};
		var parametriZahteve = {
		    "ehrId": ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: "user"
		};
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
		        console.log("Vnos podatkov uspesen");
		    },
		    error: function(err) {
		    	console.log("Vnos podatkov ni bil uspešen");
		    }
		});
}
function pridobiPodatke(ehrId){
	var sessId = getSessionId();
	var ehrID = $('#ID').val();
	var index = document.getElementById("select").selectedIndex;

	if(index == 0){
		alert("Prosimo izberite uporabnika in poskusite ponovno!")
	}
	//dodati moramo tudi meritve vitalnih znakov
	if(index == 4){
		var ID = $('#ID').val();
		var visina = $('#visina').val();
		var teza = $('#teza').val();
		var sisT = $('#sisT').val();
		var diaT = $('#diaT').val();
		if(ID != "" && visina != "" && teza != "" && sisT != "" && diaT != ""){
		dodajMeritve(ID, visina, teza, sisT, diaT);
		}
		else{
			alert("Preverite vnosna polja in poskusite ponovno");
		}
	}
	if(ehrID == ""){
		alert("Manjka EHR ID!");
	}
	$.ajax({
                    url: baseUrl + "/view/" + ehrID + "/" + "weight",
                    type: 'GET',
                    headers: {
                        "Ehr-Session": sessId
                    },
		success: function(teza){
	$.ajax({
                    url: baseUrl + "/view/" + ehrID + "/" + "height",
                    type: 'GET',
                    headers: {
                        "Ehr-Session": sessId
                    },	
		success: function(visina){
			izracunaj(visina, teza);
		},
		error: function(){
					console.log("napaka");
					}
		});
					},
		error: function(){
					console.log("napaka");
					}
		});
}
function izracunaj(visina, teza){
	var dVisina = parseInt(visina[0].height, 10);
	var dTeza = parseInt(teza[0].weight, 10)
	var BMR = (10*dTeza) + 6.25*dVisina - 5*42 + 5;
	console.log(BMR);
	$('#rezultat').append("<BR><p>Bazalni metabolizem je <strong>" +BMR +"<strong></p>");
	globBMR = BMR;
	narisiGraf(BMR);
	document.getElementById("deli").style.visibility = "visible";
	document.getElementById("twitter").style.visibility = "visible";
	civkni(BMR);
}

function narisiGraf(BMR){
	var ctx = document.getElementById('myChart').getContext('2d');
	var myBarChart = new Chart(ctx, {
    type: 'horizontalBar',
    data: {
    	labels: ["Bazalni metabolizem"],
        datasets: [{
            label: "BMR",
            backgroundColor: 'rgb(255, 99, 132)',
            borderColor: 'rgb(255, 99, 132)',
            data: [BMR],
        }]
    },
    options: {}
});
}
function civkni(){
var url = "http://google.com";
var stavek = $('rezultat').text();
console.log(globBMR);
var text = "Moj bazalni metabolizem je "+globBMR+ "! via https://ois-anzeha.c9users.io";
//window.open('http://twitter.com/share?url='+encodeURIComponent(url)+'&text='+encodeURIComponent(text), '', 'left=0,top=0,width=550,height=450,personalbar=0,toolbar=0,scrollbars=0,resizable=0');
window.open('http://twitter.com/share?url=&text='+encodeURIComponent(text), '', 'left=0,top=0,width=550,height=450,personalbar=0,toolbar=0,scrollbars=0,resizable=0');
}